#-*- coding: utf-8 -*-
from flask import Flask, render_template, request, send_file
from openpyxl import load_workbook
from cStringIO import StringIO
from random import randrange
from google.appengine.ext import ndb

class WinningEntity(ndb.Model):
    winning_date = ndb.DateProperty(auto_now_add=True)
    winning_rank = ndb.IntegerProperty()
    winning_name = ndb.StringProperty()
    winning_email = ndb.StringProperty()
    winning_product = ndb.StringProperty()

app = Flask(__name__)

@app.route('/')
def jpub_attachment():
    return render_template("file_upload.html")

@app.route("/", methods=["POST"])
def jpub_prize_winning():
    prize_file_obj = StringIO()

    prize_file = request.files['prize_file']
    prize_file.save(prize_file_obj)

    prize_file_name = prize_file.filename
    prize_file.close()

    prize_file_obj.seek(0)

    prize_workbook = load_workbook(filename=prize_file_obj)

    # 경품 응모자 읽어서 저장해두기
    subscriber_sheet = prize_workbook['Sheet1']
    subscribers = []
    subscribers_dup_cnt = 0

    # 응모자 읽기(응모자명-A|이메일-B)
    for row in subscriber_sheet.rows:
        subscribers.append((row[0].value, row[1].value))

        # 루프를 끝내기 위한 방법으로 중복자 수를 담아둔다.
        if subscribers.count((row[0].value, row[1].value)) > 1:
            subscribers_dup_cnt += 1

    # 이벤트 상품 시트
    prize_winning_sheet = prize_workbook['Sheet2']
    prize_rank_dict = dict()

    # 상품 읽기(순위-A|상품명-B|당첨자 수-C)
    for row in prize_winning_sheet.rows:
        prize_rank_dict[int(row[0].value)] = dict(name=row[1].value,
                                                  winning_cnt=int(row[2].value))

    # 중복 당첨을 막기 위해 이메일을 기준으로 담아둔다.
    prized_person_list = set()

    # 결과: 순위 / 응모자 명 / 응모자 이메일 / 상품명
    congratulation_prize_sheet = prize_workbook['Sheet3']
    congratulation_prize_sheet.cell('A1').value = u'순위'
    congratulation_prize_sheet.cell('B1').value = u'응모자명'
    congratulation_prize_sheet.cell('C1').value = u'응모자 이메일'
    congratulation_prize_sheet.cell('D1').value = u'상품명'

    # 순위 순으로 당첨자를 뽑는다.
    excel_row_num = 2
    for rank_num in prize_rank_dict.keys():
        # 몇 명을 당첨시킬지 가져옵니다.
        rdy_winning_cnt = prize_rank_dict[rank_num]['winning_cnt']

        while True: # 중복 당첨자가 나올 수 있으므로 rdy_winning_cnt가 0이 될때까지 수행
            if rdy_winning_cnt == 0: # 당첨자 수가 다 뽑히면 루프 종료
                break

            # 응모자가 모두 뽑힌 경우(중복자를 제외하고) 루프를 종료해야 한다.
            # 뽑힌 사람의 수와 (전체 응모자 수 - 중복된 사람 수(이름과 이메일))이 같으면 루프를 종료해야 한다.
            # 원래 기본조건은 이메일 중복이 없었다.
            if len(prized_person_list) == (len(subscribers) - subscribers_dup_cnt):
                break

            # 당첨자 뽑기
            temp_winning_person = subscribers[randrange(0, len(subscribers))]

            # 이미 당첨된 사람인지 확인해서 당첨된 사람이면 루프 반복하기
            if temp_winning_person[1] in prized_person_list:
                continue

            # 당첨자 목록에 담아두기
            prized_person_list.add(temp_winning_person[1])

            # 당첨자 기록하기
            congratulation_prize_sheet.\
                cell("A{0}".format(excel_row_num)).value = unicode(rank_num)
            congratulation_prize_sheet.cell("B{0}".format(excel_row_num)).\
                value = unicode(temp_winning_person[0])
            congratulation_prize_sheet.cell("C{0}".format(excel_row_num)).\
                value = unicode(temp_winning_person[1])
            congratulation_prize_sheet.cell("D{0}".format(excel_row_num)).\
                value = unicode(prize_rank_dict[rank_num]["name"])

            # 당첨자 정보를 데이터스토어에 저장하기
            winning_member = WinningEntity(winning_rank=rank_num,
                winning_name = unicode(temp_winning_person[0]),
                winning_email = unicode(temp_winning_person[1]),
                winning_product = unicode(prize_rank_dict[rank_num]["name"]))
            winning_member.put()

            # excel row number 증가시키기
            excel_row_num += 1

            # 당첨자 수 빼주기
            rdy_winning_cnt -= 1

    resultIO = StringIO()
    result = prize_workbook.save(resultIO)
    resultIO.seek(0)

    return send_file(resultIO, mimetype="application/zip", as_attachment=True,
                     attachment_filename=u"result.xlsx")

@app.route("/winning_list")
def winning_list():
    winning_query = WinningEntity.query().order(WinningEntity.winning_rank).fetch()

    return render_template("winning_list.html", winning_query = winning_query)

@app.template_filter('date_format')
def datetime_convert(dt):
    format_string = "%Y-%m-%d"
    
    return dt.strftime(format_string)